:- consult("decisionTree.pl").
:- consult("sampleTree.pl").

/**
 * Classification for new sample or input from input user
 * Check if Tree is already cached or built
 * If so, no need to re-build the tree
 */
classify(Input, Goal) :-
    cacheTree(Tree), !,
    traverseTree(Tree, Input, Goal).

/**
 * Build the tree if cacheTree is not present
 * Traverse the decision tree and find the goal
 */
classify(Input, Goal) :-
    decisionTree(Tree),
    traverseTree(Tree, Input, Goal).

% If any cacheTree/1 fact is found, then there's no need to continue further.
trainTree :-
    cacheTree(_), !.

% If there is no cacheTree/1 in the database, use existing training data to make a tree model.
trainTree :-
    \+ cacheTree(_),
    decisionTree(_).
