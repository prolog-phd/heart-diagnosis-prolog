function submit(event) {
    const optType = ["female", "cholesterol", "glucose", "smoke", "alcohol", "active"];
    const body = {};
    let invalidForm = false;
    
    optType.forEach(type => {
        const rbs = document.querySelectorAll(`input[name=${type}]`);
        for (const rb of rbs) {
            if (rb.checked) {
                body[type] = rb.value == "yes" ? true : false;
                break;
            }
        }
        if (body[type] == undefined) {
            invalidForm = true;
        }
    });

    const name      = document.querySelector("#name");
    const age       = document.querySelector("#age");
    const height    = document.querySelector("#height");
    const weight    = document.querySelector("#weight");
    const systolic  = document.querySelector("#systolic");
    const diastolic = document.querySelector("#diastolic");

    body.name       = `'${name.value}'`;
    body.age        = 365 * Number(age.value);
    body.height     = Number(height.value);
    body.weight     = Number(weight.value);
    body.systolic   = Number(systolic.value);
    body.diastolic  = Number(diastolic.value);

    if (!body.name || !body.age || !body.height || !body.weight || !body.systolic || !body.diastolic) {
        invalidForm = true;
    }

    if (invalidForm) {
        alert("Semua pertanyaan harus diisi!");
        return false;
    }

    console.log(body);

    fetch("/classify", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(body)
    })
        .then(response => response.json())
        .then(data => {
            const resultString = data.answer? "Positif" : "Negatif";
            const hasil = document.querySelector("#hasil");
            hasil.style.display = "block";
            hasil.innerHTML = `Hasil: ${resultString}`;
        })
        .catch(err => {
            console.log(err);
        });
}
