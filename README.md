# Cardiovascular Disease Prediction with ID3 Decision Tree Learning

## Group PHD

- Rizkhi Pramudya Hastiputra (1706039660)
- Rahmat Fadhilah (1706074902)
- Farhan Azyumardhi Azmi (1706979234)

## Table of Contents

- [Cardiovascular Disease Prediction with ID3 Decision Tree Learning](#cardiovascular-disease-prediction-with-id3-decision-tree-learning)
  - [Short Description](#short-description)
  - [Running the Prolog Program Directly](#running-the-prolog-program-directly)
  - [Installation and Running the Whole Application](#installation-and-running-the-whole-application)
    - [Non-Docker Method](#non-docker-method)
      - [Prerequisites](#prerequisites)
      - [Installation](#installation)
      - [Running the Application Without Docker](#running-the-application-without-docker)
    - [Docker Method](#docker-method)
      - [Prerequisite](#prerequisite)
      - [Running the Application With Docker](#running-the-application-with-docker)
  - [Internal API Documentation](#internal-api-documentation)
    - [General](#general)
    - [Classify Input](#classify-input)
    - [Train Decision Tree](#train-decision-tree)
    - [Retrieve Existing Decision Tree](#retrieve-existing-decision-tree)
  - [References](#references)

## Short Description

This web-based application asks user to input some relevant physical attributes from the user. Then, using decision tree built with [ID3 algorithm](https://en.wikipedia.org/wiki/ID3_algorithm) in Prolog, the application predicts whether the user has risks or potential of cardiovascular disease. The original dataset was taken from [Kaggle: Cardiovascular Disease Dataset](https://www.kaggle.com/sulianova/cardiovascular-disease-dataset).

## Running the Prolog Program Directly

1. Run the Prolog program directly with `swipl`.

    `swipl main.pl`.
2. To query the initial decision tree:

    `?- cacheTree(T).`
3. To train a new decision tree:

    ```prolog
    ?- retract(cacheTree(T)).
    ?- trainTree.
    ```

4. To classify a new data:

    - Sample 1

        - Data
            - Name: Alice
            - Age: 30 years or 1950 days
            - Gender: Female
            - Height: 170 cm
            - Weight: 60 kg
            - Systolic Blood Pressure: 130 mmHg
            - Diastolic Blood Pressure: 88 mmHg
            - Cholesterol Level: Above normal
            - Blood Sugar Level: Above normal
            - Smoke Status: Yes
            - Alcohol Consumption Status: Yes
            - Physically Active: Yes

        - Query

            ```prolog
            ?- assert(name('Alice')).
            ?- assert(age('Alice', 1950)).
            ?- assert(female('Alice')).
            ?- assert(height('Alice', 170)).
            ?- assert(weight('Alice', 60.0)).
            ?- assert(systolic('Alice', 130)).
            ?- assert(diastolic('Alice', 88)).
            ?- assert(cholesterol('Alice')).
            ?- assert(glucose('Alice')).
            ?- assert(smoke('Alice')).
            ?- assert(alcohol('Alice')).
            ?- assert(active('Alice')).

            ?- classify('Alice', RiskOfDisease).
            ```

    - Sample 2

        - Data

            - Name: Bob
            - Age: 30 years or 1950 days
            - Gender: Male
            - Height: 170 cm
            - Weight: 60 kg
            - Systolic Blood Pressure: 130 mmHg
            - Diastolic Blood Pressure: 88 mmHg
            - Cholesterol Level: Below normal
            - Blood Sugar Level: Below normal
            - Smoke Status: No
            - Alcohol Consumption Status: No
            - Physically Active: No

        - Query

            ```prolog
            ?- assert(name('Bob')).
            ?- assert(age('Bob', 1950)).
            ?- assert(height('Bob', 170)).
            ?- assert(weight('Bob', 60.0)).
            ?- assert(systolic('Bob', 130)).
            ?- assert(diastolic('Bob', 88)).

            ?- classify('Bob', RiskOfDisease).
            ```

## Installation and Running the Whole Application

The application can be installed and run with one of the two methods:

- [Non-Docker](#non-docker-method)
- [Docker](#docker-method)

### Non-Docker Method

#### Prerequisites

- Python version 3.5 or newer.
- Python 3 PIP.
- SWI-Prolog version 8.2.4 or newer (make sure `swipl` executable is on the `PATH`).

#### Installation

1. It is recommended to create a Python virtual environment and run the application within it to avoid dependency clashes with the "global" Python. Refer to this [documentation on venv](https://docs.python.org/3/library/venv.html) for a way to create a virtual environment.
2. Activate the virtual environment created from step 1.
3. Install all Python dependencies with `pip`.

    ```shell
    pip install -r requirements.txt
    ```

4. (Optional) Create a `.env` file containing environment variables from `.env.example`. Copy the `.env.example`, rename it to `.env` and adjust the environment variable based on your needs.

#### Running the Application Without Docker

Run the application with `python main.py`. Access the application from `localhost:<port>`, replace `<port>` with adjusted port from the environment variables. If it's not set, the default is port `8000`.

### Docker Method

#### Prerequisite

[Docker](https://www.docker.com/)

#### Running the Application With Docker

1. Build the Docker Image.

    ```shell
    docker build -t heart-diagnosis-prolog:latest .
    ```

2. Start and run the container.

    ```shell
    docker run -d -e SERVER_PORT=<port> -e LINE_RANGE_CSV_LOWER=<range_lower> -e LINE_RANGE_CSV_UPPER=<range_upper> -p <port>:<port> --name heart-diagnosis-prolog heart-diagnosis-prolog
    ```

    Replace each variable with:
    - `<port>` : Which port to assign for the application.
    - `<range_lower>` : Lower range of CSV data for training.
    - `<range_upper>` : Upper range of CSV data for training.

    Example

    ```shell
    docker run -d -e SERVER_PORT=8000 -e LINE_RANGE_CSV_LOWER=100 -e LINE_RANGE_CSV_UPPER=200 -p 8000:8000 --name heart-diagnosis-prolog heart-diagnosis-prolog
    ```

3. Access the application from `localhost:<port>`, replace `<port>` with adjusted port from the environment variables. If it's not set, the default is port `8000`.

## Internal API Documentation

This section documents the HTTP endpoints exposed by the Python server.

### General

- Status Codes

    | Code | Description |
    | --- | --- |
    | 200 | Request is successful |
    | 400 | Request body is malformed |
    | 500 | Internal server error |

### Classify Input

- HTTP Method: POST
- Path: `/classify`
- Request Body

    All fields are required.

    | Field Name | JSON Data Type | Note |
    | --- | --- | --- |
    | name |  string | Wrap with single quote (see example) |
    | age | number | In days (year x 365). Example: 20 years = 7300 days |
    | female | boolean | - |
    | height | number | - |
    | weight | number | Can contain decimal |
    | systolic | number | - |
    | diastolic | number | - |
    | cholesterol | boolean | - |
    | glucose | boolean | - |
    | smoke | boolean | - |
    | alcohol | boolean | - |
    | active | boolean | - |

- Example Request Body

    ```json
    {
        "name": "'John Wick'",
        "age": 16568,
        "female": false,
        "height": 170,
        "weight": 70.0,
        "systolic": 120,
        "diastolic": 80,
        "cholesterol": false,
        "glucose": false,
        "smoke": true,
        "alcohol": true,
        "active": true
    }
    ```

- Response Body
    - Positive
        - Fields
            | Field Name | JSON Data Type |
            | --- | --- |
            | answer | boolean |
        - Example

            ```json
            {
                "answer": true
            }
            ```

    - Negative
        - Fields
            | Field Name | JSON Data Type |
            | --- | --- |
            | errors | array of string |
    - Example

        ```json
        {
            "errors": [
                "name is required",
                "age is required"
            ]
        }
        ```

### Train Decision Tree

- HTTP Method: POST
- Path: `/train`
- Response Body
    - Fields
        | Field Name | JSON Data Type |
        | --- | --- |
        | messages | array of string |

    - Example

        ```json
        {
            "messages": ["successfully build tree model"]
        }
        ```

        ```json
        {
            "messages": ["fail to build tree model"]
        }
        ```

### Retrieve Existing Decision Tree

- HTTP Method: GET
- Path: `/tree`
- Response Body
    - Fields
        | Field Name | JSON Data Type |
        | --- | --- |
        | tree | string |

    - Example

        ```json
        {
            "tree": "tree(active, tree(alcohol, leaf(yes), tree(smoke, leaf(no), tree(glucose, leaf(yes), tree(cholesterol, leaf(yes), leaf(no))))), tree(alcohol, leaf(no), leaf(yes)))."
        }
        ```

# References

The prolog's source code is adapted from ID3 Prolog https://github.com/ignaciomosca/id3-prolog.