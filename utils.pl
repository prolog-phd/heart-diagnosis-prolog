% Find the sum from a list of numbers.
listSum([], 0).
listSum([Head | Tail], Sum) :- 
    listSum(Tail, Now), 
    Sum is Now + Head.
 
% Find the average from a list of numbers.
listAverage([], 0).
listAverage(List, Average) :- 
    length(List, Length), 
    Length =\= 0, 
    listSum(List, Total), 
    Average is Total / Length.