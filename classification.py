class Classification:
    def __init__(self, prolog):
        self.prolog = prolog

    def train(self):
        self.prolog.retractall("cacheTree(T)")
        return next(self.prolog.query("trainTree"))

    def show_tree(self):
        tree = list(self.prolog.query("cacheTree(T)"))
        if tree:
            return tree[0]["T"]
        return ""

    def solve(self, attributes):
        name = attributes["name"]
        predicates = {
            "name": "name({name})".format(name=name),
            "age": "age({name}, {age})".format(name=name, age=attributes["age"]),
            "height": "height({name}, {height})".format(
                name=name, height=attributes["height"]
            ),
            "weight": "weight({name}, {weight})".format(
                name=name, weight=attributes["weight"]
            ),
            "systolic": "systolic({name}, {systolic})".format(
                name=name, systolic=attributes["systolic"]
            ),
            "diastolic": "diastolic({name}, {diastolic})".format(
                name=name, diastolic=attributes["diastolic"]
            ),
        }
        if attributes["female"]:
            predicates["female"] = "female({name})".format(name=name)
        if attributes["cholesterol"]:
            predicates["cholesterol"] = "cholesterol({name})".format(name=name)
        if attributes["glucose"]:
            predicates["glucose"] = "glucose({name})".format(name=name)
        if attributes["smoke"]:
            predicates["smoke"] = "smoke({name})".format(name=name)
        if attributes["alcohol"]:
            predicates["alcohol"] = "alcohol({name})".format(name=name)
        if attributes["active"]:
            predicates["active"] = "active({name})".format(name=name)

        for predicate in predicates.values():
            self.prolog.assertz(predicate)

        query = "classify({name}, X)".format(name=name)
        answers = list(self.prolog.query(query))
        for predicate in predicates.values():
            self.prolog.retract(predicate)
        return answers
