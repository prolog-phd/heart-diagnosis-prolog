FROM swipl:stable

RUN apt update && \
    apt install -y python3-pip

COPY requirements.txt /tmp/
RUN pip3 install -r /tmp/requirements.txt

COPY . /app
WORKDIR /app

CMD python3 main.py