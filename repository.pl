:- consult("database.pl").

% Find an unary (one argument) predicate given its functor and the sole argument.
callUnary(Functor, Argument) :- 
    Fact =.. [Functor, Argument], 
    call(Fact). 

% Find a binary (two arguments) predicate given its functor and both arguments.
callBinary(Functor, Argument1, Argument2) :-
     Fact=.. [Functor, Argument1, Argument2], 
     call(Fact).
 
% Return a list of samples from the database.
getAllSamples(Samples) :- findall(X, (name(X), goal(Goal), goalOf(X, Goal)), Samples).

% Return a list of attributes from the database.
getAllAttributes(Attributes) :- findall(X, (attribute(X)), Attributes).

% Dividing goals into a two list based on their goal (yes or no) Left is for yes and right is for no
divideBasedOnGoals(Goals, Left, Right) :-
    findall(L, (member(L, Goals), goal(L) == goal(yes)), Left),
    findall(R, (member(R, Goals), goal(R) == goal(no)), Right).

% Find goals of given samples and sorting the results based on yes or no
getGoal(Samples, Goals) :-
    findall(Goal, (member(Dia, Samples), name(Dia), goalOf(Dia, Goal)), Result),
    sort(Result, Goals).

% Find all samples goals with the same goal either it is yes or no
sameGoal([Sample], Goal) :-
    name(Sample),
    goalOf(Sample, Goal).
sameGoal([Sample1, Sample2|Rest], Goal) :-
    name(Sample1),
    name(Sample2),
    goalOf(Sample1, Goal),
    goalOf(Sample2, Goal),
    sameGoal([Sample2|Rest], Goal).

% Find all samples and filtering based on their goal
filterByGoal(Peoples, Goal, Result) :-
    findall(Dia, (member(Dia, Peoples), goal(Goal), name(Dia), goalOf(Dia, Goal)), Result).

/** 
 * Determine the most goals from samples given 
 * If the Left list is have more length than Right list
 * Then the most goal is "yes"
 * Otherwise it the most goal is "no"
 */
mostGoal(Samples, Goal) :-
    length(Samples, LengthSamples),
    LengthSamples > 0,
    goal(Goal),
    getGoal(Samples, Goals),
    divideBasedOnGoals(Goals, Left, Right),
    length(Left, LengthLeft),
    length(Right, LengthRight),
    LengthLeft > LengthRight, !,
    Goal = yes.
mostGoal(Samples, Goal) :-
    length(Samples, LengthSamples),
    LengthSamples > 0,
    goal(Goal),
    getGoal(Samples, Goals),
    divideBasedOnGoals(Goals, Left, Right),
    length(Left, LengthLeft),
    length(Right, LengthRight),
    LengthLeft =< LengthRight,
    Goal = no.
