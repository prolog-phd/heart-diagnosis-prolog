import http.server
import os
import json
from dotenv import load_dotenv
from jsonschema.validators import Draft7Validator as SchemaValidator
from pyswip.prolog import Prolog
from classification import Classification
from csv_to_pl import csv_to_database

load_dotenv()

LINE_RANGE_CSV_LOWER = int(os.getenv("LINE_RANGE_CSV_LOWER", "0"))
LINE_RANGE_CSV_UPPER = int(os.getenv("LINE_RANGE_CSV_UPPER", "100"))

LINE_RANGE = (LINE_RANGE_CSV_LOWER, LINE_RANGE_CSV_UPPER)

SERVER_PORT = int(os.getenv("SERVER_PORT", "8000"))
REQUEST_BODY_SCHEMA = {
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "age": {"type": "number"},
        "female": {"type": "boolean"},
        "height": {"type": "number"},
        "weight": {"type": "number"},
        "systolic": {"type": "number"},
        "diastolic": {"type": "number"},
        "cholesterol": {"type": "boolean"},
        "glucose": {"type": "boolean"},
        "smoke": {"type": "boolean"},
        "alcohol": {"type": "boolean"},
        "active": {"type": "boolean"},
    },
}
REQUEST_BODY_SCHEMA["required"] = list(REQUEST_BODY_SCHEMA["properties"].keys())

prolog = Prolog()
prolog.consult("main.pl")
classification = Classification(prolog)


class APIHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        self.response_body = {}
        self.errors = None
        request_path_split = self.path.split("/")
        if len(request_path_split) > 1 and request_path_split[1] == "tree":
            self.show_tree()
            self.send_header("Content-Type", "application/json")
            self.end_headers()
            self.wfile.write(json.dumps(self.response_body).encode("utf-8"))
        else:
            return super().do_GET()

    def do_POST(self):
        self.response_body = {}
        self.errors = None
        request_path_split = self.path.split("/")

        try:
            if len(request_path_split) > 1 and request_path_split[1] == "classify":
                self.classify()
            elif len(request_path_split) > 1 and request_path_split[1] == "train":
                self.train()
            else:
                self.send_response(404)
                self.response_body["errors"] = ["no matching route found"]
        except Exception as e:
            print(str(e))
            self.send_response(500)
            self.response_body["errors"] = ["internal server error"]

        self.send_header("Content-Type", "application/json")
        self.end_headers()
        self.wfile.write(json.dumps(self.response_body).encode("utf-8"))

    def validate_request_body(self, request_body):
        validator = SchemaValidator(REQUEST_BODY_SCHEMA)
        errors = list(validator.iter_errors(request_body))
        return list(map(lambda err: err.message, errors))

    def classify(self):
        content_length = int(self.headers["Content-Length"])
        request_body = json.loads(self.rfile.read(content_length).decode("utf-8"))
        print(
            "Receiving request body: {request_body}".format(request_body=request_body)
        )
        errors = self.validate_request_body(request_body)
        if errors:
            self.send_response(400)
            self.response_body["errors"] = errors
        else:
            answer = classification.solve(request_body)[0]["X"]
            self.send_response(200)
            self.response_body["answer"] = True if answer == "yes" else False

    def train(self):
        train_result = classification.train()
        self.send_response(200)
        if train_result == {}:
            self.response_body["messages"] = ["successfully build tree model"]
        else:
            self.response_body["messages"] = ["fail to build tree model"]

    def show_tree(self):
        tree = classification.show_tree()
        self.send_response(200)
        self.response_body["tree"] = tree


csv_to_database(LINE_RANGE)

try:
    server = http.server.HTTPServer(("", SERVER_PORT), APIHandler)
    print("Serving HTTP Server at port {SERVER_PORT}".format(SERVER_PORT=SERVER_PORT))
    server.serve_forever()
except KeyboardInterrupt:
    server.server_close()
