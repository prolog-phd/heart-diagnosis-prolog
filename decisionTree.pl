:- use_module(library(clpq)).
:- consult("repository.pl").
:- consult("utils.pl").
:- dynamic cacheTree/1.

/**
 * Finding optimal entropy
 * The smallest entropy = the biggest information gain
 * If we found nothing from all value that have above average
 * That means all value is below average
 * We can determine easily with boolean value yes or no (find the most goal of people that have below average)
 */
findOptimal(Samples, Attribute, _, Average, Result) :-
   numberAttribute(Attribute),
   findall(Sample, (numberAttribute(Attribute), member(Sample, Samples), callBinary(Attribute, Sample, X1), X1 > Average), GreaterThanAverage),
   length(GreaterThanAverage, LengthGreaterThanAverage),
   LengthGreaterThanAverage =:= 0, !,
   Result is 0.
/**
 * Finding optimal entropy
 * The smallest entropy = the biggest information gain
 * If we found nothing from all value that have below average
 * That means all value is above average
 * We can determine easily with boolean value yes or no (find the most goal of people that have above average)
 */
findOptimal(Samples, Attribute, _, Average, Result) :-
   numberAttribute(Attribute),
   findall(Sample, (numberAttribute(Attribute), member(Sample, Samples), callBinary(Attribute, Sample, X2), X2 =< Average), LessThanAverage),
   length(LessThanAverage, LengthLessThanAverage),
   LengthLessThanAverage =:= 0, !,
   Result is 0.
/**
 * Finding optimal entropy
 * In this rule, we find the optimal way to determine the threshold is it below average or above average
 * We find all value of sample that have above average and below average
 * We filter that by their goal
 * We take the minimum value after the filter
 * This case is Right (Greater than) is smaller then Left side
 */
findOptimal(Samples, Attribute, Goal, Average, Result) :-
   numberAttribute(Attribute),
   findall(Sample, (numberAttribute(Attribute), member(Sample, Samples), callBinary(Attribute, Sample, X1), X1 > Average), GreaterThanAverage),
   findall(Sample, (numberAttribute(Attribute), member(Sample, Samples), callBinary(Attribute, Sample, X2), X2 =< Average), LessThanAverage),
   filterByGoal(GreaterThanAverage, Goal, GreaterThanFiltered),
   filterByGoal(LessThanAverage, Goal, LessThanFiltered),
   length(GreaterThanFiltered, LengthGreaterThanFiltered),
   length(LessThanFiltered, LengthLessThanFiltered),
   length(GreaterThanAverage, LengthGreaterThanAverage),
   length(LessThanAverage, LengthLessThanAverage),
   LengthLessThanAverage > 0,
   LengthGreaterThanAverage > 0,
   Left is LengthLessThanFiltered / LengthLessThanAverage,
   Right is LengthGreaterThanFiltered / LengthGreaterThanAverage,
   Left >= Right, !,
   Result is Right.
/**
 * Finding optimal entropy
 * In this rule, we find the optimal way to determine the threshold is it below average or above average
 * We find all value of sample that have above average and below average
 * We filter that by their goal
 * We take the minimum value after the filter
 * This case is Left (Less than) is smaller then Right side
 */
findOptimal(Samples, Attribute, Goal, Average, Result) :-
   numberAttribute(Attribute),
   findall(Sample, (numberAttribute(Attribute), member(Sample, Samples), callBinary(Attribute, Sample, X1), X1 > Average), GreaterThanAverage),
   findall(Sample, (numberAttribute(Attribute), member(Sample, Samples), callBinary(Attribute, Sample, X2), X2 =< Average), LessThanAverage),
   filterByGoal(GreaterThanAverage, Goal, GreaterThanFiltered),
   filterByGoal(LessThanAverage, Goal, LessThanFiltered),
   length(GreaterThanFiltered, LengthGreaterThanFiltered),
   length(LessThanFiltered, LengthLessThanFiltered),
   length(GreaterThanAverage, LengthGreaterThanAverage),
   length(LessThanAverage, LengthLessThanAverage),
   LengthLessThanAverage > 0,
   LengthGreaterThanAverage > 0,
   Left is LengthLessThanFiltered / LengthLessThanAverage,
   Right is LengthGreaterThanFiltered / LengthGreaterThanAverage,
   Left < Right,
   Result is Left.

/**
 * Calculate the value of the entropy.
 * In this case, if the list of people is empty, the value will be zero.
 */
calculateEntropy([], _, _, 0).

/**
 * Calculate the value of the entropy.
 * In this case, the attribute is a numeric attribute.
 * So the the value that will be analyze is based on the average value.
 */
calculateEntropy(Peoples, Goal, Target, Value) :-
   attribute(Target),
   goal(Goal),
   numberAttribute(Target),
   findall(X1, (numberAttribute(Target), member(People, Peoples), callBinary(Target, People, X1)), Weights),
   listAverage(Weights, AverageWeight),
   findOptimal(Peoples, Target, Goal, AverageWeight, OptimalSolution),
   Value is OptimalSolution.

/**
 * Calculate the value of the entropy.
 * In this case, the attribute is a not-numeric attribute.
 * So the the value that will be analyze is based on the average value.
 * If the length of partition data is zero, then the value is zero.
 */ 
calculateEntropy(Peoples, Goal, Target, Value) :-
   attribute(Target),
   goal(Goal),
   \+ numberAttribute(Target),
   findall(X, (attribute(Target), member(X, Peoples), callUnary(Target, X)), Partition),
   length(Partition, Length),
   Length =:= 0,
   Value is 0.

/**
 * Calculate the value of the entropy.
 * In this case, the attribute is a not-numeric attribute.
 * So the the value that will be analyze is based on the average value.
 * When the length of partition data is more than zero, and the length of
 * The filtered people by its goal (yes or no) divided by the total length of peoples.
 */  
calculateEntropy(Peoples, Goal, Target, Value) :-
   attribute(Target),
   goal(Goal),
   \+ numberAttribute(Target),
   findall(X, (attribute(Target), member(X, Peoples), callUnary(Target, X)), Partition),
   filterByGoal(Partition, Goal, FilteredByGoal),
   length(Partition, LengthReal),
   length(FilteredByGoal, LengthFiltered),
   LengthReal > 0,
   Value is LengthFiltered / LengthReal.
 
/**
 * Sum the value of the entropy calculation.
 * In this case, if the goal is empty, the sum value is zero.
 */
sum([], _, _, _, 0).

/**
 * Sum the value of the entropy calculation.
 * In this case, if the goal is only one, the sum value is
 * the increment of previous value and the current value for
 * the goal.
 */
sum([Goal], Peoples, Target, ValueBefore, Sum) :-
   calculateEntropy(Peoples, Goal, Target, Value),
   Value > 0,
   {Value = 2^LogaritmicValue},
   Sum is (ValueBefore + Value * LogaritmicValue).

/**
 * Sum the value of the entropy calculation.
 * In this case, if the goal is only one, and the
 * value for the goal is 0, the sum is 0.
 */
sum([Goal], Peoples, Target, _, Sum) :-
   calculateEntropy(Peoples, Goal, Target, Value),
   Value =:= 0,
   Sum is 0.

/**
 * Sum the value of the entropy calculation.
 * In this case, if the goal is more than one,
 * the overall value is the previous value + current value for the
 * first goal. Then call a recursive sum function for
 * the following goals using the updated overall value. 
 */
sum([Goal|RestGoal], Peoples, Target, ValueBefore, Sum) :-
   calculateEntropy(Peoples, Goal, Target, Value),
   Value > 0,
   {Value = 2^LogaritmicValue},
   ValueNext is (ValueBefore + Value * LogaritmicValue),
   sum(RestGoal, Peoples, Target, ValueNext, Sum).

/**
 * Sum the value of the entropy calculation.
 * In this case, if the goal is more than one,and
 * the value for the first goal is 0, then call a
 * recursive function to the following goals without
 * updating the overall value. 
 */
sum([Goal|RestGoal], Peoples, Target, ValueBefore, Sum) :-
   calculateEntropy(Peoples, Goal, Target, Value),
   Value =:= 0,
   sum(RestGoal, Peoples, Target, ValueBefore, Sum).

/**
 * Find the entropy of the target.
 * In this case, if there is no poeple in the list and
 * the target is in attribute list, the entropy is 0. 
 */ 
entropy([], Attributes, Target, 0) :-
   attribute(Target),
   member(Target, Attributes).

/**
 * Find the entropy of the target.
 * In this case, if target is not in attribute list,
 * the entropy is 1. 
 */ 
entropy(_, Attributes, Target, 1) :-
   attribute(Target),
   \+ member(Target, Attributes).

/**
 * Find the entropy of the target.
 * In this last case, the entropy is negative of
 * the sum the entropy calculation value for some
 * people, target, and goals.
 */ 
entropy(Peoples, Attributes, Target, Entropy) :-
   attribute(Target),
   member(Target, Attributes),
   getGoal(Peoples, Goals),
   sum(Goals, Peoples, Target, 0, Sum),
   Entropy is -Sum.

/**
 * Find the attribute with the smallest entropy.
 * In this case, there is only one attribute in the list so it gets chosen.
 */
minimumEntropyAttribute(_, [ChosenAttribute], ChosenAttribute).
 
/**
 * Find the attribute with the smallest entropy.
 * In this case, entropies from the two attributes are calculated.
 * Then, choose the second attribute if its entropy is lower than the first attribute's.
 */
minimumEntropyAttribute(Samples, [CandidateAttribute1 | [CandidateAttribute2]], ChosenAttribute) :-
   attribute(CandidateAttribute1),
   attribute(CandidateAttribute2),
   entropy(Samples, [CandidateAttribute1 | [CandidateAttribute2]], CandidateAttribute1, EntropyCandidate1),
   entropy(Samples, [CandidateAttribute1 | [CandidateAttribute2]], CandidateAttribute2, EntropyCandidate2),
   EntropyCandidate1 >= EntropyCandidate2,
   ChosenAttribute = CandidateAttribute2.

/**
 * Find the attribute with the smallest entropy.
 * In this case, entropies from the two attributes are calculated.
 * Then, choose the first attribute if its entropy is lower than the second attribute's.
 */ 
minimumEntropyAttribute(Samples, [CandidateAttribute1|[CandidateAttribute2]], ChosenAttribute) :-
   attribute(CandidateAttribute1),
   attribute(CandidateAttribute2),
   entropy(Samples, [CandidateAttribute1 | [CandidateAttribute2]], CandidateAttribute1, EntropyCandidate1),
   entropy(Samples, [CandidateAttribute1 | [CandidateAttribute2]], CandidateAttribute2, EntropyCandidate2),
   EntropyCandidate1 < EntropyCandidate2,
   ChosenAttribute = CandidateAttribute1.

/**
 * Find the attribute with the smallest entropy.
 * In this case, entropies from the first two attributes are calculated.
 * Then, recursively find the candidate attribute without the first attribute if it's entropy is lower than the second's.
 */ 
minimumEntropyAttribute(Samples, [CandidateAttribute1, CandidateAttribute2 | OtherAttributes], ChosenAttribute) :-
   attribute(CandidateAttribute1),
   attribute(CandidateAttribute2),
   entropy(Samples, [CandidateAttribute1, CandidateAttribute2 | OtherAttributes], CandidateAttribute1, EntropyCandidate1),
   entropy(Samples, [CandidateAttribute1, CandidateAttribute2 | OtherAttributes], CandidateAttribute2, EntropyCandidate2),
   EntropyCandidate1 >= EntropyCandidate2,
   minimumEntropyAttribute(Samples, [CandidateAttribute2 | OtherAttributes], ChosenAttribute).
 
/**
 * Find the attribute with the smallest entropy.
 * In this case, entropies from the first two attributes are calculated.
 * Then, recursively find the candidate attribute without the second attribute if it's entropy is lower than the first's.
 */ 
minimumEntropyAttribute(Samples, [CandidateAttribute1, CandidateAttribute2 | OtherAttributes], ChosenAttribute) :-
   attribute(CandidateAttribute1),
   attribute(CandidateAttribute2),
   entropy(Samples, [CandidateAttribute1, CandidateAttribute2 | OtherAttributes], CandidateAttribute1, EntropyCandidate1),
   entropy(Samples, [CandidateAttribute1, CandidateAttribute2 | OtherAttributes], CandidateAttribute2, EntropyCandidate2),
   EntropyCandidate1 < EntropyCandidate2,
   minimumEntropyAttribute(Samples, [CandidateAttribute1|OtherAttributes], ChosenAttribute).

/**
 * There is no attributes
 * Then from given samples determine the most goal
 * And make it leaf of the tree with the value of the most goal
 */
buildTree(Samples, [], _, leaf(Goal)) :- mostGoal(Samples, Goal),!.
/**
 * There is no samples
 * Always make the leaf with the goal from parent node (the most goal)
 */
buildTree([], _, Goal, leaf(Goal)) :- !.
/**
 * If all the samples have the same goals
 * No need to find the minimum Entropy (winner attribute)
 * Just make the leaf with the same goal of samples given
 */
buildTree(Samples, _, _, Tree) :-
   sameGoal(Samples, Goal),
   Tree = leaf(Goal),!.
/**
 * If no condition match the above rules
 * Find the winner attribute (minimum entropy attribute) from the attributes given
 * This is for winner attribute that is not Numerical Attribute
 * From the winner attribute, make the tree and the leaf recursively
 * Recursively build Tree with Left Sub Tree is all the samples that have same attribute as WinnerAttribute
 * While the Right Sub Tree is all the samples that have NO same attribute as WinnerAttribute
 */
buildTree(Samples, Attributes, Goal, Tree) :-
   minimumEntropyAttribute(Samples, Attributes, WinnerAttribute),
   \+ numberAttribute(WinnerAttribute), !,
   delete(Attributes, WinnerAttribute, RemainingAttributes),
   findall(X, (attribute(WinnerAttribute), member(X, Samples), callUnary(WinnerAttribute, X)), Left),
   findall(X, (attribute(WinnerAttribute), member(X, Samples), \+ callUnary(WinnerAttribute, X)), Right),
   buildTree(Left, RemainingAttributes, Goal, SubTreeLeft), 
   buildTree(Right, RemainingAttributes, Goal, SubTreeRight),
   Tree = tree(WinnerAttribute, SubTreeLeft, SubTreeRight), !.
/**
 * If no condition match the above rules
 * Find the winner attribute (minimum entropy attribute) from the attributes given
 * This is for winner attribute that is Numerical Attribute
 * From the winner attribute, make the tree and the leaf recursively
 * Recursively build Tree with Left Sub Tree is all the samples that have same attribute as WinnerAttribute
 * We determine the Left Sub Tree by the Value of the Samples that have above Average
 * While the Right Sub Tree is all the samples that have NO same attribute as WinnerAttribute
 * We determine the Right Sub Tree by the Value of the Samples that have below Average
 */
buildTree(Samples, Attributes, Goal, Tree) :-
   minimumEntropyAttribute(Samples, Attributes, WinnerAttribute),
   numberAttribute(WinnerAttribute),
   delete(Attributes, WinnerAttribute, RemainingAttributes),
   findall(X, (numberAttribute(Node), callBinary(Node, _, X)), AllSampleValue),
   listAverage(AllSampleValue, AverageValue),
   findall(X, (attribute(WinnerAttribute), member(X, Samples), callBinary(WinnerAttribute, X, Value), Value >= AverageValue), Left),
   findall(X, (attribute(WinnerAttribute), member(X, Samples), callBinary(WinnerAttribute, X, Value), Value < AverageValue), Right),
   buildTree(Left, RemainingAttributes, Goal, SubTreeLeft),
   buildTree(Right, RemainingAttributes, Goal, SubTreeRight),
   Tree = tree(WinnerAttribute, SubTreeLeft, SubTreeRight), !.

/**
 * Make the decision tree
 * Find all available samples that have goalOf in the database
 * Find all available attributes
 * From given samples, determine the most goal of samples
 * Build the decision tree with given informations
 * When the build of decision tree is finish, Cache it.
 */
decisionTree(Tree) :-
   getAllSamples(Samples),
   getAllAttributes(Attributes),
   mostGoal(Samples, Goal),
   buildTree(Samples, Attributes, Goal, Tree),
   assert(cacheTree(Tree)).

/**
 * If the sample or people already on database
 * No need to traverse, just return the value of goalOf
 */
traverseTree(_, Input, Goal) :-
   name(Input),
   goalOf(Input, Goal), !.
/**
 * If we are already on the leaf of branch that have the same goal
 * Then we are finished traversing the decision tree
 * Return the goal
 */
traverseTree(leaf(Goal), Input, Goal) :- name(Input), !.
/**
 * If we are currently not in the leaf, we still need to traverse
 * This rule is for node (attribute) that are numerical
 * If numerical, then we need to extract the real value
 * And compare it to the average, if it greater than average then we traverse to the left
 * If it is lesser, we traverse to the right
 */
traverseTree(tree(Node, Left, _), Input, Goal) :-
   name(Input),
   numberAttribute(Node),
   callBinary(Node, Input, Value),
   findall(X, (numberAttribute(Node), callBinary(Node, Name, X), Name \= Input), AllSamplesValue),
   listAverage(AllSamplesValue, AverageValue),
   Value >= AverageValue,
   traverseTree(Left, Input, Goal), !.
/**
 * If we are currently not in the leaf, we still need to traverse
 * This rule is for node (attribute) that are numerical
 * If numerical, then we need to extract the real value
 * And compare it to the average, if it lesser than average then we traverse to the right
 */
traverseTree(tree(Node, _, Right), Input, Goal) :-
   name(Input),
   numberAttribute(Node),
   callBinary(Node, Input, Value),
   findall(X, (numberAttribute(Node), callBinary(Node, Name, X), Name \= Input), AllSamplesValue),
   listAverage(AllSamplesValue, AverageValue),
   Value < AverageValue,
   traverseTree(Right, Input, Goal), !.
/**
 * If we are currently not in the leaf, we still need to traverse
 * This rule is for node (attribute) that are boolean
 * Simply check if the attribute of person is True or False
 * If True then we traverse to the left
 */
traverseTree(tree(Node, Left, _), Input, Goal) :-
   name(Input),
   \+ numberAttribute(Node),
   callUnary(Node, Input),
   traverseTree(Left, Input, Goal), !.
/**
 * If we are currently not in the leaf, we still need to traverse
 * This rule is for node (attribute) that are boolean
 * Simply check if the attribute of person is True or False
 * If False then we traverse to the right
 */
traverseTree(tree(Node, _, Right), Input, Goal) :-
   name(Input),
   \+ numberAttribute(Node),
   traverseTree(Right, Input, Goal), !.
