import csv

HEADERS = [
    "name",
    "age",
    "female",
    "height",
    "weight",
    "systolic",
    "diastolic",
    "cholesterol",
    "glucose",
    "smoke",
    "alcohol",
    "active",
    "cardio",
]

CSV_FILE = "cardio_train.csv"


def write_header(file_output):
    file_output.write(":- dynamic attribute/1.\n")
    file_output.write(":- dynamic numberAttribute/2.\n")
    file_output.write(":- dynamic goal/1.\n")
    file_output.write(":- dynamic goalOf/2.\n")
    for header in HEADERS:
        if header == "cardio":
            continue
        elif header in ["age", "height", "weight", "systolic", "diastolic"]:
            file_output.write(":- dynamic {header}/2.\n".format(header=header))
        else:
            file_output.write(":- dynamic {header}/1.\n".format(header=header))
    file_output.write("\n")
    file_output.write("goal(no).\n")
    file_output.write("goal(yes).\n")
    file_output.write("\n")


def write_fact(name, header, data, out):
    if header in ["age", "height", "weight", "systolic", "diastolic"]:
        out.write(
            "{header}({name}, {data}).\n".format(header=header, name=name, data=data)
        )
    elif header == "female":
        if data == "1":
            out.write("female({name}).\n".format(name=name))
    elif header in ["cholesterol", "glucose", "smoke", "alcohol", "active"]:
        if data == "1":
            out.write("{header}({name}).\n".format(header=header, name=name))
    elif header == "cardio":
        target = "yes" if data == "1" else "no"
        out.write("goalOf({name}, {target}).\n".format(name=name, target=target))
    else:
        out.write("name({name}).\n".format(name=name))


def write_body(file_output, line_range):
    for pointer in range(len(HEADERS)):
        header = HEADERS[pointer]
        line_count = 0
        csv_content = open(CSV_FILE, "r")
        csv_reader = csv.reader(csv_content, delimiter=";")
        for row in csv_reader:
            if line_count == 0:
                pass
            elif line_range[0] <= line_count <= line_range[1]:
                name = row[0]
                data = row[pointer]
                write_fact(name, header, data, file_output)
            elif line_count > line_range[1]:
                break
            line_count += 1
        file_output.write("\n")


def write_attribute(file_output):
    for header in HEADERS:
        if header == "cardio":
            continue
        file_output.write("attribute({header}).\n".format(header=header))
    file_output.write("\n")
    for header in ["age", "height", "weight", "systolic", "diastolic"]:
        file_output.write("numberAttribute({header}).\n".format(header=header))
    file_output.write("\n")


def csv_to_database(line_range):
    OUTFILE = "database.pl"
    file_output = open(OUTFILE, "w+")
    write_header(file_output)
    write_attribute(file_output)
    write_body(file_output, line_range)
