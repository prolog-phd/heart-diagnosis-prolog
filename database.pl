:- dynamic attribute/1.
:- dynamic numberAttribute/2.
:- dynamic goal/1.
:- dynamic goalOf/2.
:- dynamic name/1.
:- dynamic age/2.
:- dynamic female/1.
:- dynamic height/2.
:- dynamic weight/2.
:- dynamic systolic/2.
:- dynamic diastolic/2.
:- dynamic cholesterol/1.
:- dynamic glucose/1.
:- dynamic smoke/1.
:- dynamic alcohol/1.
:- dynamic active/1.

goal(no).
goal(yes).

attribute(name).
attribute(age).
attribute(female).
attribute(height).
attribute(weight).
attribute(systolic).
attribute(diastolic).
attribute(cholesterol).
attribute(glucose).
attribute(smoke).
attribute(alcohol).
attribute(active).

numberAttribute(age).
numberAttribute(height).
numberAttribute(weight).
numberAttribute(systolic).
numberAttribute(diastolic).

name(0).
name(1).
name(2).
name(3).
name(4).
name(8).
name(9).
name(12).
name(13).
name(14).
name(15).
name(16).
name(18).
name(21).
name(23).
name(24).
name(25).
name(27).
name(28).
name(29).
name(30).
name(31).
name(32).
name(33).
name(35).
name(36).
name(37).
name(38).
name(39).
name(40).
name(42).
name(43).
name(44).
name(45).
name(46).
name(47).
name(49).
name(51).
name(52).
name(53).
name(54).
name(56).
name(57).
name(58).
name(59).
name(60).
name(61).
name(62).
name(63).
name(64).
name(65).
name(66).
name(67).
name(68).
name(69).
name(70).
name(71).
name(72).
name(73).
name(74).
name(77).
name(79).
name(81).
name(83).
name(86).
name(87).
name(88).
name(90).
name(92).
name(94).
name(95).
name(96).
name(97).
name(100).
name(103).
name(104).
name(105).
name(106).
name(107).
name(108).
name(109).
name(111).
name(113).
name(114).
name(115).
name(116).
name(117).
name(119).
name(121).
name(122).
name(123).
name(124).
name(125).
name(126).
name(127).
name(129).
name(131).
name(132).
name(133).
name(134).

age(0, 18393).
age(1, 20228).
age(2, 18857).
age(3, 17623).
age(4, 17474).
age(8, 21914).
age(9, 22113).
age(12, 22584).
age(13, 17668).
age(14, 19834).
age(15, 22530).
age(16, 18815).
age(18, 14791).
age(21, 19809).
age(23, 14532).
age(24, 16782).
age(25, 21296).
age(27, 16747).
age(28, 17482).
age(29, 21755).
age(30, 19778).
age(31, 21413).
age(32, 23046).
age(33, 23376).
age(35, 16608).
age(36, 14453).
age(37, 19559).
age(38, 18085).
age(39, 14574).
age(40, 21057).
age(42, 18291).
age(43, 23186).
age(44, 14605).
age(45, 20652).
age(46, 21940).
age(47, 20404).
age(49, 18328).
age(51, 17976).
age(52, 23388).
age(53, 18126).
age(54, 19848).
age(56, 18274).
age(57, 21475).
age(58, 20556).
age(59, 19764).
age(60, 17471).
age(61, 18207).
age(62, 18535).
age(63, 16864).
age(64, 16045).
age(65, 18238).
age(66, 18338).
age(67, 19575).
age(68, 14507).
age(69, 19679).
age(70, 21787).
age(71, 17407).
age(72, 22748).
age(73, 15901).
age(74, 20431).
age(77, 19105).
age(79, 20960).
age(81, 20330).
age(83, 22587).
age(86, 20649).
age(87, 21752).
age(88, 19148).
age(90, 22099).
age(92, 19739).
age(94, 20882).
age(95, 23589).
age(96, 21874).
age(97, 23433).
age(100, 21934).
age(103, 16039).
age(104, 20484).
age(105, 20397).
age(106, 21865).
age(107, 18241).
age(108, 20370).
age(109, 16591).
age(111, 19029).
age(113, 19642).
age(114, 19570).
age(115, 22636).
age(116, 18352).
age(117, 21909).
age(119, 19663).
age(121, 23204).
age(122, 19480).
age(123, 23230).
age(124, 19557).
age(125, 18752).
age(126, 22821).
age(127, 15946).
age(129, 21076).
age(131, 19258).
age(132, 18410).
age(133, 21860).
age(134, 17363).

female(1).
female(2).
female(4).
female(8).
female(9).
female(13).
female(14).
female(15).
female(21).
female(25).
female(27).
female(28).
female(31).
female(32).
female(35).
female(36).
female(37).
female(38).
female(42).
female(43).
female(44).
female(45).
female(47).
female(51).
female(53).
female(54).
female(56).
female(59).
female(60).
female(61).
female(64).
female(65).
female(66).
female(68).
female(69).
female(71).
female(72).
female(74).
female(77).
female(83).
female(87).
female(92).
female(94).
female(95).
female(97).
female(100).
female(104).
female(106).
female(107).
female(109).
female(111).
female(113).
female(114).
female(115).
female(116).
female(117).
female(121).
female(123).
female(124).
female(129).
female(132).
female(134).

height(0, 168).
height(1, 156).
height(2, 165).
height(3, 169).
height(4, 156).
height(8, 151).
height(9, 157).
height(12, 178).
height(13, 158).
height(14, 164).
height(15, 169).
height(16, 173).
height(18, 165).
height(21, 158).
height(23, 181).
height(24, 172).
height(25, 170).
height(27, 158).
height(28, 154).
height(29, 162).
height(30, 163).
height(31, 157).
height(32, 158).
height(33, 156).
height(35, 170).
height(36, 153).
height(37, 156).
height(38, 159).
height(39, 166).
height(40, 169).
height(42, 155).
height(43, 169).
height(44, 159).
height(45, 160).
height(46, 173).
height(47, 163).
height(49, 175).
height(51, 164).
height(52, 162).
height(53, 165).
height(54, 157).
height(56, 178).
height(57, 171).
height(58, 159).
height(59, 154).
height(60, 162).
height(61, 162).
height(62, 168).
height(63, 175).
height(64, 170).
height(65, 160).
height(66, 169).
height(67, 166).
height(68, 165).
height(69, 152).
height(70, 165).
height(71, 171).
height(72, 165).
height(73, 172).
height(74, 164).
height(77, 159).
height(79, 165).
height(81, 187).
height(83, 170).
height(86, 169).
height(87, 148).
height(88, 170).
height(90, 171).
height(92, 152).
height(94, 157).
height(95, 155).
height(96, 179).
height(97, 156).
height(100, 157).
height(103, 180).
height(104, 158).
height(105, 188).
height(106, 160).
height(107, 173).
height(108, 164).
height(109, 159).
height(111, 155).
height(113, 158).
height(114, 152).
height(115, 178).
height(116, 156).
height(117, 156).
height(119, 166).
height(121, 151).
height(122, 172).
height(123, 160).
height(124, 164).
height(125, 173).
height(126, 168).
height(127, 185).
height(129, 158).
height(131, 165).
height(132, 165).
height(133, 170).
height(134, 167).

weight(0, 62.0).
weight(1, 85.0).
weight(2, 64.0).
weight(3, 82.0).
weight(4, 56.0).
weight(8, 67.0).
weight(9, 93.0).
weight(12, 95.0).
weight(13, 71.0).
weight(14, 68.0).
weight(15, 80.0).
weight(16, 60.0).
weight(18, 60.0).
weight(21, 78.0).
weight(23, 95.0).
weight(24, 112.0).
weight(25, 75.0).
weight(27, 52.0).
weight(28, 68.0).
weight(29, 56.0).
weight(30, 83.0).
weight(31, 69.0).
weight(32, 90.0).
weight(33, 45.0).
weight(35, 68.0).
weight(36, 65.0).
weight(37, 59.0).
weight(38, 78.0).
weight(39, 66.0).
weight(40, 74.0).
weight(42, 105.0).
weight(43, 71.0).
weight(44, 60.0).
weight(45, 73.0).
weight(46, 82.0).
weight(47, 55.0).
weight(49, 95.0).
weight(51, 70.0).
weight(52, 72.0).
weight(53, 70.0).
weight(54, 62.0).
weight(56, 68.0).
weight(57, 69.0).
weight(58, 63.0).
weight(59, 50.0).
weight(60, 64.0).
weight(61, 107.0).
weight(62, 69.0).
weight(63, 70.0).
weight(64, 69.0).
weight(65, 75.0).
weight(66, 84.0).
weight(67, 85.0).
weight(68, 77.0).
weight(69, 79.0).
weight(70, 73.0).
weight(71, 76.0).
weight(72, 90.0).
weight(73, 84.0).
weight(74, 64.0).
weight(77, 58.0).
weight(79, 75.0).
weight(81, 115.0).
weight(83, 85.0).
weight(86, 71.0).
weight(87, 80.0).
weight(88, 69.0).
weight(90, 97.0).
weight(92, 76.0).
weight(94, 53.0).
weight(95, 57.0).
weight(96, 95.0).
weight(97, 58.0).
weight(100, 77.0).
weight(103, 90.0).
weight(104, 75.0).
weight(105, 105.0).
weight(106, 68.0).
weight(107, 82.0).
weight(108, 74.0).
weight(109, 49.0).
weight(111, 64.0).
weight(113, 75.0).
weight(114, 110.0).
weight(115, 93.0).
weight(116, 55.0).
weight(117, 75.0).
weight(119, 94.0).
weight(121, 92.0).
weight(122, 87.0).
weight(123, 82.0).
weight(124, 103.0).
weight(125, 76.0).
weight(126, 80.0).
weight(127, 88.0).
weight(129, 53.0).
weight(131, 65.0).
weight(132, 99.0).
weight(133, 100.0).
weight(134, 71.0).

systolic(0, 110).
systolic(1, 140).
systolic(2, 130).
systolic(3, 150).
systolic(4, 100).
systolic(8, 120).
systolic(9, 130).
systolic(12, 130).
systolic(13, 110).
systolic(14, 110).
systolic(15, 120).
systolic(16, 120).
systolic(18, 120).
systolic(21, 110).
systolic(23, 130).
systolic(24, 120).
systolic(25, 130).
systolic(27, 110).
systolic(28, 100).
systolic(29, 120).
systolic(30, 120).
systolic(31, 130).
systolic(32, 145).
systolic(33, 110).
systolic(35, 150).
systolic(36, 130).
systolic(37, 130).
systolic(38, 120).
systolic(39, 120).
systolic(40, 130).
systolic(42, 120).
systolic(43, 140).
systolic(44, 110).
systolic(45, 130).
systolic(46, 140).
systolic(47, 120).
systolic(49, 120).
systolic(51, 130).
systolic(52, 130).
systolic(53, 140).
systolic(54, 110).
systolic(56, 110).
systolic(57, 140).
systolic(58, 120).
systolic(59, 170).
systolic(60, 140).
systolic(61, 150).
systolic(62, 120).
systolic(63, 120).
systolic(64, 120).
systolic(65, 100).
systolic(66, 150).
systolic(67, 150).
systolic(68, 135).
systolic(69, 130).
systolic(70, 125).
systolic(71, 90).
systolic(72, 120).
systolic(73, 140).
systolic(74, 180).
systolic(77, 110).
systolic(79, 180).
systolic(81, 130).
systolic(83, 120).
systolic(86, 120).
systolic(87, 130).
systolic(88, 130).
systolic(90, 150).
systolic(92, 160).
systolic(94, 110).
systolic(95, 120).
systolic(96, 150).
systolic(97, 110).
systolic(100, 140).
systolic(103, 140).
systolic(104, 130).
systolic(105, 120).
systolic(106, 140).
systolic(107, 140).
systolic(108, 140).
systolic(109, 120).
systolic(111, 120).
systolic(113, 120).
systolic(114, 160).
systolic(115, 130).
systolic(116, 100).
systolic(117, 150).
systolic(119, 140).
systolic(121, 130).
systolic(122, 120).
systolic(123, 140).
systolic(124, 140).
systolic(125, 150).
systolic(126, 160).
systolic(127, 133).
systolic(129, 110).
systolic(131, 110).
systolic(132, 150).
systolic(133, 120).
systolic(134, 120).

diastolic(0, 80).
diastolic(1, 90).
diastolic(2, 70).
diastolic(3, 100).
diastolic(4, 60).
diastolic(8, 80).
diastolic(9, 80).
diastolic(12, 90).
diastolic(13, 70).
diastolic(14, 60).
diastolic(15, 80).
diastolic(16, 80).
diastolic(18, 80).
diastolic(21, 70).
diastolic(23, 90).
diastolic(24, 80).
diastolic(25, 70).
diastolic(27, 70).
diastolic(28, 70).
diastolic(29, 70).
diastolic(30, 80).
diastolic(31, 80).
diastolic(32, 85).
diastolic(33, 60).
diastolic(35, 90).
diastolic(36, 100).
diastolic(37, 90).
diastolic(38, 80).
diastolic(39, 80).
diastolic(40, 70).
diastolic(42, 80).
diastolic(43, 90).
diastolic(44, 70).
diastolic(45, 85).
diastolic(46, 90).
diastolic(47, 80).
diastolic(49, 80).
diastolic(51, 90).
diastolic(52, 80).
diastolic(53, 90).
diastolic(54, 70).
diastolic(56, 80).
diastolic(57, 90).
diastolic(58, 60).
diastolic(59, 80).
diastolic(60, 90).
diastolic(61, 90).
diastolic(62, 80).
diastolic(63, 80).
diastolic(64, 70).
diastolic(65, 60).
diastolic(66, 100).
diastolic(67, 100).
diastolic(68, 90).
diastolic(69, 90).
diastolic(70, 90).
diastolic(71, 60).
diastolic(72, 80).
diastolic(73, 90).
diastolic(74, 90).
diastolic(77, 70).
diastolic(79, 90).
diastolic(81, 90).
diastolic(83, 80).
diastolic(86, 80).
diastolic(87, 90).
diastolic(88, 80).
diastolic(90, 100).
diastolic(92, 100).
diastolic(94, 70).
diastolic(95, 80).
diastolic(96, 90).
diastolic(97, 70).
diastolic(100, 90).
diastolic(103, 90).
diastolic(104, 90).
diastolic(105, 80).
diastolic(106, 90).
diastolic(107, 90).
diastolic(108, 85).
diastolic(109, 70).
diastolic(111, 70).
diastolic(113, 80).
diastolic(114, 90).
diastolic(115, 90).
diastolic(116, 60).
diastolic(117, 90).
diastolic(119, 90).
diastolic(121, 90).
diastolic(122, 80).
diastolic(123, 100).
diastolic(124, 90).
diastolic(125, 90).
diastolic(126, 100).
diastolic(127, 89).
diastolic(129, 70).
diastolic(131, 70).
diastolic(132, 110).
diastolic(133, 80).
diastolic(134, 80).

cholesterol(0).
cholesterol(3).
cholesterol(4).
cholesterol(13).
cholesterol(14).
cholesterol(15).
cholesterol(16).
cholesterol(18).
cholesterol(21).
cholesterol(23).
cholesterol(24).
cholesterol(25).
cholesterol(27).
cholesterol(28).
cholesterol(29).
cholesterol(30).
cholesterol(31).
cholesterol(33).
cholesterol(37).
cholesterol(38).
cholesterol(39).
cholesterol(40).
cholesterol(44).
cholesterol(45).
cholesterol(47).
cholesterol(49).
cholesterol(51).
cholesterol(52).
cholesterol(53).
cholesterol(54).
cholesterol(56).
cholesterol(57).
cholesterol(58).
cholesterol(60).
cholesterol(62).
cholesterol(64).
cholesterol(65).
cholesterol(66).
cholesterol(67).
cholesterol(69).
cholesterol(70).
cholesterol(71).
cholesterol(72).
cholesterol(73).
cholesterol(74).
cholesterol(77).
cholesterol(81).
cholesterol(83).
cholesterol(86).
cholesterol(87).
cholesterol(88).
cholesterol(92).
cholesterol(94).
cholesterol(95).
cholesterol(96).
cholesterol(97).
cholesterol(100).
cholesterol(104).
cholesterol(105).
cholesterol(106).
cholesterol(107).
cholesterol(108).
cholesterol(109).
cholesterol(111).
cholesterol(113).
cholesterol(114).
cholesterol(115).
cholesterol(116).
cholesterol(121).
cholesterol(122).
cholesterol(123).
cholesterol(125).
cholesterol(126).
cholesterol(129).
cholesterol(131).
cholesterol(132).
cholesterol(133).

glucose(0).
glucose(1).
glucose(2).
glucose(3).
glucose(4).
glucose(9).
glucose(13).
glucose(14).
glucose(15).
glucose(16).
glucose(18).
glucose(21).
glucose(23).
glucose(24).
glucose(25).
glucose(28).
glucose(29).
glucose(30).
glucose(31).
glucose(33).
glucose(35).
glucose(36).
glucose(37).
glucose(38).
glucose(39).
glucose(42).
glucose(43).
glucose(44).
glucose(45).
glucose(46).
glucose(47).
glucose(49).
glucose(51).
glucose(52).
glucose(53).
glucose(54).
glucose(56).
glucose(57).
glucose(58).
glucose(59).
glucose(60).
glucose(61).
glucose(62).
glucose(63).
glucose(64).
glucose(65).
glucose(66).
glucose(67).
glucose(69).
glucose(70).
glucose(72).
glucose(73).
glucose(74).
glucose(77).
glucose(79).
glucose(81).
glucose(83).
glucose(86).
glucose(87).
glucose(88).
glucose(90).
glucose(94).
glucose(95).
glucose(96).
glucose(97).
glucose(100).
glucose(104).
glucose(105).
glucose(106).
glucose(107).
glucose(108).
glucose(109).
glucose(111).
glucose(113).
glucose(114).
glucose(115).
glucose(116).
glucose(121).
glucose(122).
glucose(123).
glucose(125).
glucose(126).
glucose(129).
glucose(131).
glucose(132).
glucose(133).
glucose(134).

smoke(23).
smoke(29).
smoke(52).
smoke(73).
smoke(74).
smoke(90).

alcohol(23).
alcohol(81).
alcohol(134).

active(0).
active(1).
active(3).
active(9).
active(12).
active(13).
active(15).
active(16).
active(21).
active(23).
active(27).
active(29).
active(30).
active(31).
active(32).
active(33).
active(35).
active(36).
active(37).
active(38).
active(39).
active(42).
active(43).
active(44).
active(47).
active(49).
active(51).
active(52).
active(53).
active(56).
active(57).
active(58).
active(59).
active(60).
active(61).
active(63).
active(64).
active(66).
active(67).
active(68).
active(69).
active(71).
active(73).
active(74).
active(77).
active(79).
active(81).
active(83).
active(86).
active(87).
active(88).
active(90).
active(92).
active(95).
active(96).
active(97).
active(100).
active(105).
active(106).
active(107).
active(109).
active(111).
active(113).
active(114).
active(116).
active(117).
active(119).
active(123).
active(125).
active(126).
active(127).
active(129).
active(131).
active(134).

goalOf(0, no).
goalOf(1, yes).
goalOf(2, yes).
goalOf(3, yes).
goalOf(4, no).
goalOf(8, no).
goalOf(9, no).
goalOf(12, yes).
goalOf(13, no).
goalOf(14, no).
goalOf(15, no).
goalOf(16, no).
goalOf(18, no).
goalOf(21, no).
goalOf(23, no).
goalOf(24, yes).
goalOf(25, no).
goalOf(27, no).
goalOf(28, no).
goalOf(29, no).
goalOf(30, no).
goalOf(31, no).
goalOf(32, yes).
goalOf(33, no).
goalOf(35, yes).
goalOf(36, no).
goalOf(37, no).
goalOf(38, no).
goalOf(39, no).
goalOf(40, no).
goalOf(42, yes).
goalOf(43, yes).
goalOf(44, no).
goalOf(45, yes).
goalOf(46, yes).
goalOf(47, no).
goalOf(49, no).
goalOf(51, no).
goalOf(52, yes).
goalOf(53, yes).
goalOf(54, no).
goalOf(56, yes).
goalOf(57, yes).
goalOf(58, yes).
goalOf(59, yes).
goalOf(60, yes).
goalOf(61, yes).
goalOf(62, no).
goalOf(63, no).
goalOf(64, no).
goalOf(65, no).
goalOf(66, yes).
goalOf(67, yes).
goalOf(68, yes).
goalOf(69, yes).
goalOf(70, no).
goalOf(71, no).
goalOf(72, yes).
goalOf(73, yes).
goalOf(74, yes).
goalOf(77, no).
goalOf(79, yes).
goalOf(81, no).
goalOf(83, no).
goalOf(86, yes).
goalOf(87, yes).
goalOf(88, no).
goalOf(90, yes).
goalOf(92, yes).
goalOf(94, yes).
goalOf(95, yes).
goalOf(96, yes).
goalOf(97, no).
goalOf(100, yes).
goalOf(103, no).
goalOf(104, yes).
goalOf(105, yes).
goalOf(106, yes).
goalOf(107, yes).
goalOf(108, yes).
goalOf(109, no).
goalOf(111, no).
goalOf(113, yes).
goalOf(114, yes).
goalOf(115, yes).
goalOf(116, no).
goalOf(117, yes).
goalOf(119, yes).
goalOf(121, yes).
goalOf(122, no).
goalOf(123, no).
goalOf(124, yes).
goalOf(125, yes).
goalOf(126, no).
goalOf(127, no).
goalOf(129, no).
goalOf(131, no).
goalOf(132, yes).
goalOf(133, yes).
goalOf(134, yes).

